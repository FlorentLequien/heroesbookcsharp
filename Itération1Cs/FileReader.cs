﻿using HeroesBook.Model;
using System;

namespace HeroesBook
{
    public class FileReader
    {
        String _title;
        Progress _progress = new Progress();
        Paragraph[] paragraphesJson;
        Paragraph[] paragraphesDuLivre;

        public FileReader(/*json*/)
        {
            //Identifier Le titre, tous les paragraphes puis les créer; "Foreach String..."
            _title = ""; //Deviendra json.Title

            for (int i = 0; i < paragraphesJson.Length; i++ /*Deviendra le nombre de paragraphes du json*/)
            {
                paragraphesDuLivre[i] = paragraphesJson[i];
            }

        }

        public Book CreateBook()
            => new Book(_title, paragraphesDuLivre);
    }
}
