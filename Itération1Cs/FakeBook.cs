﻿using HeroesBook.Model;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Action = HeroesBook.Model.Action;

namespace HeroesBook
{
    public class FakeBook 
    {
        private string _title;
        Dictionary<int, Paragraph> _paragraphes = new Dictionary<int, Paragraph>();


        private static Action actionParagraph4 = new Action("renvoie au chapitre 4", 4);
        private static Action actionParagraph6 = new Action("renvoie au chapitre 6", 6);
        private static Action actionParagraph5 = new Action("renvoie au chapitre 5", 5);
        private static Action actionParagraph3 = new Action("renvoie au chapitre 3", 3);
        private static Action actionParagraph2 = new Action("renvoie au chapitre 2", 2);
        private static Action actionParagraph1 = new Action("renvoie au chapitre 1", 1);


        private Paragraph paragraphe0 = new Paragraph("Je suis le paragraphe numéro 0, Aller au chapitre 2 (1), aller au chapitre 6 (2)", 0, actionParagraph2, actionParagraph6);
        private Paragraph paragraphe1 = new Paragraph("Je suis le paragraphe numéro 1, Aller au chapitre 4 (1), aller au chapitre 5 (2), aller au chapitre 6 (3)", 1, actionParagraph4, actionParagraph5,actionParagraph6);
        private Paragraph paragraphe2 = new Paragraph("Je suis le paragraphe numéro 2, aller au paragraphe 4 (1), aller au paragraphe 1 (2), aller au paragraphe 3 (3)", 2, actionParagraph4, actionParagraph1, actionParagraph3);
        private Paragraph paragraphe3 = new Paragraph("Je suis le paragraphe numéro 3, aller au chapitre 6 (1), aller au chapitre 2 (2)", 3, actionParagraph6, actionParagraph2);
        private Paragraph paragraphe4 = new Paragraph("Je suis le paragraphe numéro 4, VOUS ETES MORT", 4);
        private Paragraph paragraphe5 = new Paragraph("je suis le paragraphe numéro 5, aller au paragraphe 4 (1), aller au paragraphe 1 (2)", 5, actionParagraph4, actionParagraph1);
        private Paragraph paragraphe6 = new Paragraph("Je suis le paragraphe numéro 6, aller au paragraphe 3 (1), aller au chapitre 5 (2)", 3, actionParagraph3, actionParagraph5);

        public void ConstructFakeBook()
        {
            Paragraph[] paragraphes = new Paragraph[10];
            paragraphes[0] = paragraphe0;
            paragraphes[1] = paragraphe1;
            paragraphes[2] = paragraphe2;
            paragraphes[3] = paragraphe3;
            paragraphes[4] = paragraphe4;
            paragraphes[5] = paragraphe5;
            paragraphes[6] = paragraphe6;

            _title = "Le nain dans la vallée";
            
            for (int i = 0; i < paragraphes.Length; i++)
            {
                _paragraphes.Add(i, paragraphes[i]);
            }
        }

        public string Title
        {
            get => _title;
        }

        

        public int CountParagraph()
            => _paragraphes.Count;


        public Paragraph getParagraph(int pNumber)
            => _paragraphes.ContainsKey(pNumber) ? _paragraphes[pNumber] : null;

        public void Reset()
        {
            _title = "";
            _paragraphes.Clear();
        }
    }   

}

