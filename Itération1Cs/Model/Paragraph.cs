﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace HeroesBook.Model
{
    public class Paragraph
    {
        private int _number;
        private string _texte;
        private List<Action> _action = new List<Action>();

        public Paragraph(string texte, int numeroParagraphe, params Action[] actions)
        {

            foreach (Action action in actions)
            {
                _action.Add(action);
            }

            _texte = texte;
            _number = numeroParagraphe;
        }

        public int ParagraphNumber
        {
            get => _number;
            set { _number = value; }
        }

        public string Texte
        {
            get => _texte;
            set { _texte = value; }
        }

        public List<Action> Action
        {
            get => _action;
            set { _action = value; }
        }

        public Boolean IsLast()
            => _action.Count == 0;



        public Action GetAction(int currentChoice)
        => _action.ElementAt(currentChoice-1);
    }
}
