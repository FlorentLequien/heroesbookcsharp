﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeroesBook.Model
{
    public class Action
    {
        private string _description;
        private int _numToChapter;
        
        public Action(string description, int numChapter)
        {
            _description = description;
            _numToChapter = numChapter;
        }

        public string Description
        {
            get => _description;
            set { _description = value; }
        }

        public int NumToChapter
        {
            get => _numToChapter;
            set { _numToChapter = value; }
        }
    }
}
