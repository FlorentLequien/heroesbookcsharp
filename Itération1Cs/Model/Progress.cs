﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace HeroesBook.Model
{
    public class Progress
    {
        private List<int> _previousParagraphs;

        public Progress()
        {
            _previousParagraphs = new List<int>();
        }

        public List<int> Int
        {
            get => _previousParagraphs;
            set { _previousParagraphs = value; }
        }

        public void AddChoosenParagraph(int i)
        { 
            _previousParagraphs.Add(i);
        }

        public int Count()
        => _previousParagraphs.Count();


        public int GoBack()
        {
            int toReturn = _previousParagraphs.ElementAt(_previousParagraphs.Count() - 1);
            _previousParagraphs.Remove(_previousParagraphs.ElementAt(_previousParagraphs.Count() - 1));
            return toReturn;
            
        }

        public Boolean MoreThanOneAction()
        {
            return _previousParagraphs.Count() >= 1;
        }

        public int GetParagraph(int index)
        {
            return _previousParagraphs.ElementAt(index);
        }

        public void Reset()
        {
            _previousParagraphs.Clear();
        }
    }
}
