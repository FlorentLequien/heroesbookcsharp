﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeroesBook.Model
{
    public class Book
    {
        private string _title;
        Dictionary<int, Paragraph> _paragraphes = new Dictionary<int, Paragraph>();

        public Book(string title, params Paragraph[] paragraphes)
        {
            _title = title;


            for(int i=0; i<paragraphes.Length; i++) { 
                _paragraphes.Add(i, paragraphes[i]);
            }
        }

        public string Title
        {
            get => _title;
        }

        public int CountParagraph()
            => _paragraphes.Count;
        

        public Paragraph GetParagraph(int pNumber)
            => _paragraphes.ContainsKey(pNumber) ? _paragraphes[pNumber] : null;

        public void Reset()
        {
            _title = "";
            _paragraphes.Clear();
        }
    }
}
