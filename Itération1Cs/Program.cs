﻿using HeroesBook;
using HeroesBook.Model;
using System;


namespace HeroesBook
{
    public class Program
    {
        private static FrontController _frontController = new FrontController();
        
        static void Main(string[] args)
        {
            _frontController.StartReading();
            _frontController.RunBook();
        }
    }
}
