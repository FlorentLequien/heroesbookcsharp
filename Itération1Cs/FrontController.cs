﻿using HeroesBook.Model;
using HeroesBook.consoles;
using System;
using System.Linq;


namespace HeroesBook
{
    class FrontController
    {
        FakeBook _book;
        ConsoleClass _console = new ConsoleClass();
        Progress _progress = new Progress();

        public void StartReading()
        {
            int entree = 0;
            _progress.Reset();

            do
            {
                _console.WriteString("Lancer la lecture (1) \n Sortie du programme (2)");
                entree = _console.ReadInteger();
            } while (entree == 0 || entree > 2);

            if (entree == 1)
            {
                //FileReader fileReader = new FileReader();
                //Book book = fileReader.CreateBook();
                _book = new FakeBook();
                _book.ConstructFakeBook();
                Console.WriteLine(_book.Title);
            }
            else if (entree == 2)
            {
                _console.WriteString("A bientôt !!");
                System.Environment.Exit(0);
            }
        }

        public void RunBook()
        {
            int currentChoice =0;
            int numberNextParagraph=0;
            do
            {
                _console.WriteString(_book.getParagraph(1).Texte);
                _progress.AddChoosenParagraph(1);
                try
                {
                    currentChoice = _console.ReadInteger();
                    numberNextParagraph = _book.getParagraph(1).GetAction(currentChoice).NumToChapter;
                }
                catch (ArgumentOutOfRangeException)
                {
                    _console.WriteString("veuillez entrer une valeur correcte");
                }
            } while (currentChoice == 0);
            Boolean currentParagraphIsLast = false;
            Boolean backToMenu = false;
            string choice = "";


            while (backToMenu == false)
            {
                try
                {
                    if (currentParagraphIsLast == true)
                    {
                        _console.WriteString("FIN DE L'HISTOIRE \n");
                        _console.WriteString(_book.getParagraph(numberNextParagraph).Texte + "\n Revenir en arrière ? oui (o) non (n)");
                        choice = _console.ReadString();
                        if (choice.Equals("o"))
                        {
                            currentChoice = 0;
                            currentParagraphIsLast = false;
                        }
                        else if (choice.Equals("n"))
                        {
                            backToMenu = true;
                            _console.WriteString("Revenir au menu principal? (o)ui/ (n)on ");
                            EndBook(_console.ReadString());
                        }
                    }
                    else
                    {
                        _console.WriteString(_book.getParagraph(numberNextParagraph).Texte + "\n Revenir en arrière ? (0) \n");
                        currentChoice = _console.ReadInteger();
                    }


                    if (currentChoice == 0)
                    {
                        numberNextParagraph = _progress.GoBack();
                        Paragraph currentParagraph = _book.getParagraph(numberNextParagraph);

                    }
                    else
                    {
                        _progress.AddChoosenParagraph(numberNextParagraph);
                        numberNextParagraph = _book.getParagraph(numberNextParagraph).GetAction(currentChoice).NumToChapter;
                        Paragraph currentParagraph = _book.getParagraph(numberNextParagraph);
                        if (currentParagraph.IsLast())
                        {
                            currentParagraphIsLast = true;
                        }
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    _console.WriteString("Veuillez entrer un choix valide");
                }
            }
        }


        public void EndBook(String choice)
        {
            if (choice.Equals("o"))
            {
                _book.Reset();
                _progress.Reset();
                StartReading();
                RunBook();
            }
            else
            {
                _console.WriteString("A bientôt !!");
                System.Environment.Exit(0);
            }
        }
    }
}