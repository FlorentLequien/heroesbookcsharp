﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace HeroesBook.consoles
{
    class ConsoleClass
    {

        public void WriteString(string o)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(o);
        }

        public void WriteInt(int i)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(i);
        }


        public int ReadInteger()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            try
            {
                int value = int.Parse(Console.ReadLine());
                return value;
            }
            catch (System.FormatException)
            {
                Console.WriteLine("Veuillez entrer un nombre");
            }
            return -1;
        }


        public string ReadString()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            string value = Console.ReadLine();
            return value;
        }
    }
}
