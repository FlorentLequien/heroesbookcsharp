using HeroesBook.Model;
using NUnit.Framework;


namespace HeroesBookBookTest
{
    public class BookTests
    {
        private Paragraph _paragraph0;
        private Paragraph _paragraph1;
        private Paragraph _paragraph2;
        private Paragraph _paragraph3;
        private Progress _progress;
        private Book _book;

        [SetUp]
        public void Setup()
        {
            _progress = new Progress();
            _book = new Book("MonLivreDeTest", _paragraph0, _paragraph1, _paragraph2, _paragraph3);
        }

        [Test]
        public void TestCountParagraph()
        {
            Assert.AreEqual(4, _book.CountParagraph());
        }

        [Test]
        public void TestGetParagraph()
        {
            Assert.AreEqual(_book.GetParagraph(1), _paragraph0);
        }

        [Test]
        public void ResetWorks()
        { 
            _book = new Book("MonLivreDeTest", _paragraph0, _paragraph1, _paragraph2, _paragraph3);
            _book.Reset();
            Assert.AreEqual(_book.Title, "");
            Assert.AreEqual(_book.CountParagraph(), 0);
        }
    }
}