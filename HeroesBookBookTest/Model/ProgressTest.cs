﻿using HeroesBook.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace HeroesBookBookTest
{
    public class ProgressTest
    {
        private Progress _currentProgress;
        private Paragraph paragraph1;
        private Paragraph paragraph2;
        private Paragraph paragraph3;
        private Paragraph paragraph4;

        [SetUp]
        public void Setup()
        {
            _currentProgress = new Progress();
            paragraph1 = new Paragraph("Je suis le paragraphe 1", 1);
            paragraph2 = new Paragraph("Je suis le paragraphe 2", 2);
            paragraph3 = new Paragraph("je suis le paragraphe 3", 3);
            paragraph4 = new Paragraph("je suis le paragraphe 4", 4) ;
            
        }


        [Test]
        public void AddChoosenParagraphWorks()
        {
            _currentProgress.AddChoosenParagraph(paragraph1.ParagraphNumber);
            _currentProgress.AddChoosenParagraph(paragraph2.ParagraphNumber);
            Assert.AreEqual(_currentProgress.GetParagraph(0), 1);
        }


        [Test]
        public void GoBackWorks()
        {
            _currentProgress.AddChoosenParagraph(3);
            _currentProgress.AddChoosenParagraph(5);
            _currentProgress.AddChoosenParagraph(2);
            _currentProgress.AddChoosenParagraph(8);
            _currentProgress.AddChoosenParagraph(1);
            _currentProgress.GoBack();
            _currentProgress.GoBack();
            Assert.AreEqual(_currentProgress.GoBack(), 2);
        }

        [Test]
        public void GetListWorks()
        {
            _currentProgress.AddChoosenParagraph(3);
            _currentProgress.AddChoosenParagraph(5);
            _currentProgress.AddChoosenParagraph(2);
            List<int> actionList = new List<int>();
            actionList.Add(3);
            actionList.Add(5);
            actionList.Add(2);

            Assert.AreEqual(_currentProgress.Int, actionList);
        }

        [Test]
        public void GoBackWhileFirstReturnsFirst()
        {
            _currentProgress.AddChoosenParagraph(1);
            _currentProgress.AddChoosenParagraph(987);
            _currentProgress.AddChoosenParagraph(4);
            _currentProgress.AddChoosenParagraph(2);
            _currentProgress.AddChoosenParagraph(4);
            _currentProgress.GoBack();
            _currentProgress.GoBack();
            _currentProgress.GoBack();
            _currentProgress.GoBack();
            _currentProgress.GoBack();
            try
            {
                _currentProgress.GoBack();
                Assert.Fail(); // If it gets to this line, no exception was thrown
            }
            catch (ArgumentOutOfRangeException) { }
        }

        [Test]
        public void ResetWorks()
        {
            _currentProgress.AddChoosenParagraph(1);
            _currentProgress.AddChoosenParagraph(987);
            _currentProgress.AddChoosenParagraph(4);
            _currentProgress.AddChoosenParagraph(2);
            _currentProgress.AddChoosenParagraph(4);
            _currentProgress.Reset();
            Assert.AreEqual(_currentProgress.Count(), 0);
        }

        [Test]
        public void MoreThanOneActionReturnsTrue()
        {
            _currentProgress.AddChoosenParagraph(987);
            _currentProgress.AddChoosenParagraph(4);
            _currentProgress.AddChoosenParagraph(2);
            Assert.IsTrue(_currentProgress.MoreThanOneAction());
        }

        [Test]
        public void EmptyProgressReturnsFalse()
        {
            Progress progress = new Progress();
            Assert.IsFalse(progress.MoreThanOneAction());
        }
    }
}