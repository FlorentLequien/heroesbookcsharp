﻿using System;
using HeroesBook.Model;
using NUnit.Framework;
using Action = HeroesBook.Model.Action;

namespace HeroesBookBookTest
{
    public class ActionTest
    {

        [Test]
        public void GetDescriptionWorks()
        {

            Action _action = new Action("i am action 0", 10);
            Action _action1 = new Action("i am action 1", 5);

            Assert.AreEqual(_action.Description, "i am action 0");
            Assert.AreEqual(_action1.Description, "i am action 1");
        }

        [Test]
        public void SetDescriptionWorks()
        {

            Action _action = new Action("i am action 0", 10);
            Action _action1 = new Action("i am action 1", 5);

            _action.Description = "i have changed";
            Assert.AreEqual(_action.Description, "i have changed");
            Assert.AreEqual(_action1.Description, "i am action 1");
        }

        [Test]
        public void GetNumberWorks()
        {

            Action _action = new Action("i am action 0", 10);
            Action _action1 = new Action("i am action 1", 5);

            Assert.AreEqual(_action.NumToChapter, 10);
            Assert.AreEqual(_action1.NumToChapter, 5);
        }

        [Test]
        public void SetNumberWorks()
        {

            Action _action = new Action("i am action 0", 10);
            Action _action1 = new Action("i am action 1", 5);
            _action.NumToChapter = 10000;

            Assert.AreEqual(_action.NumToChapter, 10000);
            Assert.AreEqual(_action1.NumToChapter, 5);
        }
    }
}
