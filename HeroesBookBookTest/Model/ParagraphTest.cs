﻿using HeroesBook.Model;
using NUnit.Framework;


namespace HeroesBookBookTest
{
    public class ParagraphTest
    {
        private int _numeroParagraphe;
        private Action _action0;
        private Action _action1;
        private string _texte = "Le nain de la vallée était joli";
        private Paragraph paragraph0;
        private Paragraph paragraph1;

        [SetUp]
        public void Setup()
        {
            paragraph0 = new Paragraph(_texte, 1, _action0, _action1);
            paragraph1 = new Paragraph(_texte, 1);
        }

        [Test]
        public void IsLastTest()
        {
            Assert.IsFalse(paragraph0.IsLast());
            Assert.IsTrue(paragraph1.IsLast());
        }

        [Test]
        public void GetAndSetTextWorks()
        {
            Paragraph test = new Paragraph("I am a test paragraph", 1);
            test.Texte = "i changed";
            Assert.AreEqual(test.Texte, "i changed");
        }

        [Test]
        public void GetActionWorks()
        {
            Assert.AreEqual(paragraph0.GetAction(1), _action0);
        }

        [Test]
        public void setNumberWorks()
        {
            paragraph0 = new Paragraph(_texte, 1, _action0, _action1);
            paragraph0.ParagraphNumber = 10000;
            Assert.AreEqual(paragraph0.ParagraphNumber, 10000);
        }
    }
}